//
//  SharedPreferences.swift
//  Empresa App
//
//  Created by Deyvid Lopes on 03/10/20.
//

import Foundation

class SharedPreferences{
    static let preferences = SharedPreferences()
    static let sharedPreferences = UserDefaults.standard
    
    func getKeyValue (_ key : String) -> String {
        if (SharedPreferences.sharedPreferences.object(forKey: key) != nil) {
            return SharedPreferences.sharedPreferences.object(forKey: key) as! String
        } else {
            return ""
        }
    }
    func checkKey(_ key : String) -> Bool {
        if(SharedPreferences.sharedPreferences.object(forKey: key) != nil) {
            return true
        } else {
            return false
        }
    }
    
    func putKey(_ key : String , value : String) {
        SharedPreferences.sharedPreferences.set(value, forKey: key)
        SharedPreferences.sharedPreferences.synchronize()
    }
    
    func updateCoins (_ key : String , coins : Int) {
        SharedPreferences.sharedPreferences.set(coins, forKey: key)
        SharedPreferences.sharedPreferences.synchronize()
    }
    
    func deleteKey(_ key: String){
        SharedPreferences.sharedPreferences.removeObject(forKey: key)
    }
    
}
