//
//  Theme.swift
//  Empresa App
//
//  Created by Deyvid Lopes on 05/10/20.
//

import UIKit

class Theme {
    static let background = UIColor(named: "background")
    static let rosa = UIColor(named: "Rosa")
    static let rosa2 = UIColor(named: "Rosa 2")
    static let cinza1 = UIColor(named: "Cinza 1")
    static let cinza2 = UIColor(named: "Cinza 2")
    static let cinza3 = UIColor(named: "Cinza 3")
    static let cinza4 = UIColor(named: "Cinza 4")
}
