//
//  LoginApi.swift
//  Empresa App
//
//  Created by Deyvid Lopes on 03/10/20.
//

import Alamofire
import Alamofire_SwiftyJSON
import SwiftyJSON

class LoginApi {
    
    static func login(params parameters: Parameters, completion: @escaping (DataResponse<JSON>)-> ()) {

        let headers = ["content-type" : "application/json"]
        Alamofire.request(Constants.loginUrl, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers).responseSwiftyJSON { (response) in
            completion(response)
        }
    }
}
