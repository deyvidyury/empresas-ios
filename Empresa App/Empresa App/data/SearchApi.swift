//
//  SearchApi.swift
//  Empresa App
//
//  Created by Deyvid Lopes on 04/10/20.
//
import Alamofire
import Alamofire_SwiftyJSON
import SwiftyJSON
import SVProgressHUD

class SearchApi {
    
    static func search(params parameters: Parameters, completion: @escaping (DataResponse<JSON>)-> ()) {
        SVProgressHUD.show()
        let sharedPreferences = SharedPreferences()

        let headers = ["content-type" : "application/json", "access-token": sharedPreferences.getKeyValue("access-token"), "uid": sharedPreferences.getKeyValue("uid"), "client": sharedPreferences.getKeyValue("client")]
        
        var components = URLComponents(string: Constants.searchUrl)!
        components.queryItems = parameters.map { (key, value) in
            URLQueryItem(name: key, value: value as! String)
        }
        
        var request = URLRequest(url: components.url!)
        request.httpMethod = "GET"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.setValue(sharedPreferences.getKeyValue("access-token"), forHTTPHeaderField: "access-token")
        request.setValue(sharedPreferences.getKeyValue("uid"), forHTTPHeaderField: "uid")
        request.setValue(sharedPreferences.getKeyValue("client"), forHTTPHeaderField: "client")
        request.timeoutInterval = 10
        
        Alamofire.request(request).responseSwiftyJSON { (response) in
            SVProgressHUD.dismiss()
            completion(response)
        }
        
    }
}
