//
//  EmpresaDetailViewController.swift
//  Empresa App
//
//  Created by Deyvid Lopes on 05/10/20.
//

import UIKit
import Alamofire
import AlamofireImage

class EmpresaDetailViewController: UIViewController {
    var empresa: Enterprise?
    
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var desc: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = empresa?.enterprise_name
        self.setImage()
        desc.text = empresa?.description
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        desc.setContentOffset(CGPoint.zero, animated: true)
    }
    
    func setImage() {
        print("\(Constants.ioasysUrl)\(empresa?.photo ?? "")")
        Alamofire.request("\(Constants.ioasysUrl)\(empresa?.photo ?? "")", method: .get).responseImage { [weak self] (response) in
            if case .success(let image) = response.result {
                self?.photo.image = image
            }
            
        }
    }
}
