//
//  EmpresaViewCell.swift
//  Empresa App
//
//  Created by Deyvid Lopes on 05/10/20.
//

import UIKit

class EmpresaViewCell: UITableViewCell {
    
    @IBOutlet weak var EmpresaName: UILabel!
    @IBOutlet weak var view: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func layoutSubviews() {
        superview?.layoutSubviews()
        view.layer.borderColor = UIColor(named: "Cinza 1")?.cgColor
        view.layer.borderWidth = 1
        view.layer.cornerRadius = 5
    }
    
    func setLabels(name: String) {
        EmpresaName.text = name
    }
}
