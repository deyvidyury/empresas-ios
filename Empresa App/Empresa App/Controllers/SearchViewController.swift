//
//  SearchViewController.swift
//  Empresa App
//
//  Created by Deyvid Lopes on 03/10/20.
//

import UIKit
import Alamofire
import SVProgressHUD

struct Enterprize_type: Decodable {
    let id: Int?
    let enterprize_type_name: String?
}

struct Enterprise: Decodable {
    let id: Int?
    let email_enterprise: String?
    let facebook: String?
    let twitter: String?
    let linkedin: String?
    let phone: String?
//    let own_enterprise: Int?
    let enterprise_name: String?
    let photo: String?
    let description: String?
    let city: String?
    let country: String?
    let value: Float?
    let share_price: Float?
    let enterprise_type: Enterprize_type?
}

struct Enterprises: Decodable {
    let enterprises: [Enterprise]?
}

class SearchViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var resultadosLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var empresas: [Enterprise] = []
    
    var searchDebouncer: Debouncer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        searchTextField.addTarget(self, action: #selector(textFieldDidChange), for: .editingChanged)
        tableView.dataSource = self
        tableView.delegate = self
        resultadosLabel.isHidden = true
        tableView.isHidden = true
    }
    
    @objc func textFieldDidChange(sender: UITextField) {
        
        guard let searchText = sender.text else {
            return
        }
        
        let parameters: Parameters = ["name": searchText]
        
        let debouncerHandler: () -> Void = { [weak self] in
            SearchApi.search(params: parameters) { (response) in
                guard let data = response.data else {return}
                
                do {
                    let json = try JSONDecoder().decode(Enterprises.self, from: data)
                    self?.empresas = json.enterprises ?? []
                    self?.update()
                } catch let jsonErr {
                    print(jsonErr)
                }
            }
        }
        
        guard let searchDebouncer = self.searchDebouncer else {
            let debouncer = Debouncer(delay: 1.0, handler: {})
            self.searchDebouncer = debouncer
            textFieldDidChange(sender: sender)
            return
        }
        
        searchDebouncer.invalidate()
        searchDebouncer.handler = debouncerHandler
        searchDebouncer.call()
        
    }
    
    private func update() {
        if empresas.count > 0 {
            resultadosLabel.text = "\(empresas.count) resultados encontrados"
            resultadosLabel.isHidden = false
            tableView.isHidden = false
            tableView.reloadData()
        }
    }

}

extension SearchViewController {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if empresas.count == 0 {
            self.tableView.setEmptyMessage("Nenhum resultado encontrado")
        } else {
            self.tableView.restore()
        }

        return empresas.count
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return CGFloat(300)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EmpresaCell", for: indexPath) as! EmpresaViewCell
        
        cell.setLabels(name: empresas[indexPath.item].enterprise_name ?? "")
        cell.clipsToBounds = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let empresaDetailVC = storyBoard.instantiateViewController(withIdentifier: "EmpresaDetailViewController") as! EmpresaDetailViewController
        empresaDetailVC.empresa = empresas[indexPath.item]
        self.navigationController?.pushViewController(empresaDetailVC, animated: true)
    }
}
