//
//  ViewController.swift
//  Empresa App
//
//  Created by Deyvid Lopes on 03/10/20.
//

import UIKit
import Alamofire
import SVProgressHUD

class LogginViewController: UIViewController {

    @IBOutlet weak var usuario: UITextField!
    @IBOutlet weak var senha: UITextField!
    @IBOutlet weak var aviso: UILabel!
    
    let sharedPreferences = SharedPreferences()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
//        usuario.text = "testeapple@ioasys.com.br"
//        senha.text = "12341234"
    }


    @IBAction func entrar(_ sender: Any) {
        SVProgressHUD.show()
        
        let parameters: Parameters = [
            "email": usuario.text!,
            "password": senha.text!
        ]
        
        LoginApi.login(params: parameters) { (dados) in
            
            guard let access_token = dados.response?.allHeaderFields["access-token"],
                  let uid = dados.response?.allHeaderFields["uid"],
                  let client = dados.response?.allHeaderFields["client"]
            else {
                print("ERROR")
                SVProgressHUD.dismiss()
                self.aviso.isHidden = false
                return
            }
            self.aviso.isHidden = true
            SVProgressHUD.dismiss()
            self.sharedPreferences.putKey("access-token", value: access_token as! String)
            self.sharedPreferences.putKey("uid", value: uid as! String)
            self.sharedPreferences.putKey("client", value: client as! String)
            
            self.doSeque()
            
        }
    }
    
    private func doSeque() {
        self.performSegue(withIdentifier: "logged", sender: self)
    }
}

