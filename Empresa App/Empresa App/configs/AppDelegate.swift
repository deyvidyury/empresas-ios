//
//  AppDelegate.swift
//  Empresa App
//
//  Created by Deyvid Lopes on 03/10/20.
//

import UIKit
#if DEBUG
import netfox
#endif

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        #if DEBUG
        NFX.sharedInstance().start()
        #endif
        
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController : LogginViewController = storyboard.instantiateViewController(withIdentifier: "LogginViewController") as! LogginViewController
        window?.rootViewController = viewController
        window?.makeKeyAndVisible()
        return true
    }


}

