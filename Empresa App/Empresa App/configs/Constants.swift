//
//  Constants.swift
//  Empresa App
//
//  Created by Deyvid Lopes on 03/10/20.
//

import Foundation

class Constants {
    static let baseUrl = "https://empresas.ioasys.com.br/api/v1"
    
    static let ioasysUrl = "https://empresas.ioasys.com.br"
    
    static let loginUrl = "\(Constants.baseUrl)/users/auth/sign_in"
    
    static let searchUrl = "\(Constants.baseUrl)/enterprises"
}
